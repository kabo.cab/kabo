import Layout from "../components/Layout";

export default function Home() {
  return (
    <Layout page="Passenger">
      <div className="passenger-page">
        <section className="section-one">
          <div className="section-content">
            <h2>Kabo takes you to your favorite places</h2>
            <p className="text-primary">Your new driving service app</p>
            <div className="store-links">
              <div>
                <img
                  src={require("../public/assets/appstore.svg")}
                  height={50}
                />
                <span className="coming-soon">Coming Soon</span>
              </div>
              <div>
                <img
                  src={require("../public/assets/googleplay.svg")}
                  height={50}
                />
              </div>
            </div>
          </div>
          <div className="section-image">
            <img src={require("../public/assets/app_mockup_mobile.png")} />
          </div>
        </section>
      </div>
    </Layout>
  );
}
