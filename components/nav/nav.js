import React, { useRef } from "react";
import Link from "next/link";
import useScrollDetect from "../../service/useScrollDetect";

const pages = [
  {
    id: 1,
    page: "Passenger",
  },
  {
    id: 2,
    page: "Driver",
  },
  {
    id: 3,
    page: "Help",
  },
];

const Nav = ({ activePage }) => {
  const { shadow } = useScrollDetect();
  const menuRef = useRef();
  const overlayRef = useRef();

  const openMenu = () => {
    menuRef.current.style.transform = "translateX(30vw)";
    overlayRef.current.style.display = "block";
  };

  const closeMenu = () => {
    menuRef.current.style.transform = "translateX(100vw)";
    overlayRef.current.style.display = "none";
  };

  return (
    <nav className={shadow ? "box-shadow white-bg" : null}>
      <div className="navbar">
        <Link href="/">
          <div className="logo">
            <img src={require("../../public/assets/logo.png")} />
          </div>
        </Link>
        <div className="navigation">
          {pages.map((page) => (
            <button
              className={`btn btn-primary ${
                activePage === page.page && "active"
              } `}
              key={page.id}
            >
              {page.page}
            </button>
          ))}
        </div>
        <div className="menu-btn" onClick={openMenu}>
          <img src={require("../../public/assets/menu.svg")} height={28} />
        </div>
      </div>
      {/* overlay for offCanvas menu */}
      <div className="overlay" ref={overlayRef} />

      {/* overlay for offCanvas menu ends */}

      <div className="offCanvas" ref={menuRef}>
        <div className="offMenu">
          <div className="title">
            <h2>Menu</h2>
            <img
              onClick={closeMenu}
              src={require("../../public/assets/close.svg")}
              height={20}
            />
          </div>

          <div className="off-navigation">
            {pages.map((page) => (
              <button
                className={`btn btn-primary ${
                  activePage === page.page && "active"
                } `}
                key={page.id}
              >
                {page.page}
              </button>
            ))}
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
