import React from "react";
import Head from "next/head";
import Nav from "./nav/nav";

const Layout = ({ children, page }) => {
  return (
    <div>
      <Head>
        <title>{page}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Nav activePage={page} />
      <div className="layout-content">{children}</div>
    </div>
  );
};

export default Layout;
